import React from 'react'
import { FaMedium, FaEnvelope, FaTwitter, FaGithub, FaLinkedinSquare } from 'react-icons/lib/fa'
import { links } from '../../data/socialLinks'

import styles from './socialLinksStyles.module.css'

const SocialLinks = () => {
  return (
    <div className={styles.socialLinks}>
      <ul>
        <li>
          <a href={links.email}>
            <FaEnvelope />
          </a>
        </li>
        <li>
          <a href={links.github}>
            <FaGithub />
          </a>
        </li>
        <li>
          <a href={links.twitter}>
            <FaTwitter />
          </a>
        </li>
        <li>
          <a href={links.medium}>
            <FaMedium />
          </a>
        </li>
        <li>
          <a href={links.linkedin}>
            <FaLinkedinSquare />
          </a>
        </li>
      </ul>
    </div>
  )
}

export default SocialLinks
