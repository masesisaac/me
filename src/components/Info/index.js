import React from 'react'

import Avatar from '../Avatar/index'
import SocialLinks from '../SocialLinks/index'

import { me } from '../../data/about'

const Info = () => {
  return (
    <div style={{ textAlign: 'center' }}>
      <Avatar />
      <div>
        <h2>Hi, I'm {me.name} 👋</h2>
        <h3>{me.title} <span style={{fontSize: 25}}>👨🏾‍💻</span></h3>
        <SocialLinks />
      </div>
    </div>
  )
}

export default Info
