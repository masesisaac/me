export const devstack = [
  { name: 'JavaScript' },
  { name: 'Node.js' },
  { name: 'Expressjs' },
  { name: 'Hapi.js' },
  { name: 'React' },
  { name: 'React Native' },
  { name: 'Gatsbyjs' },
  { name: 'create-react-app' },
  { name: 'Expo' },
  { name: 'MongoDB / MySQL' },
]

export const technologies = [
  'Meteorjs',
  'Firebase',
  'Redux',
  'Bootstrap',
  'Sass',
  'styled-components',
  'Mocha',
  'ESLint',
  'npm',
  'Docker',
  'Kubernetes'
]
