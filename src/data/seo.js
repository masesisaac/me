const seo = {
  TITLE: 'Isaac Maseruka',
  AUTHOR: 'masesisaac',
  DESCRIPTION: 'Isaac Maseruka\'s Portfolio Website',
  TYPE: 'WEBSITE',
  TWITTER_USERNAME: '@masesisaac',
  URL: 'www.masesisaac.me',
  IMAGE_URL: 'https://i.imgur.com/6wd7Eb7.png',
}

export default seo
