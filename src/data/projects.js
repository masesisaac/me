export const projects = [
  {
    title: 'CryptoSavvy',
    type: 'Private',
    client: 'simicode',
    description: 'Automated Crypto Trading Platform',
    techStack: 'Reactjs, MobX, Bootstrap, Heroku, Nodejs, ExpressJs Socket.io, Chart.js',
    extra: null,
    link: 'http://cryptosavvy.herokuapp.com/',
    github: {
      name: 'Github',
      githubHref: 'https://github.com/simicode',
    },
  }
]
