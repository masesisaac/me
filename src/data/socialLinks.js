export const links = {
  github: 'https://github.com/masesisaac',
  twitter: 'https://twitter.com/masesisaac',
  medium: 'https://medium.com/@masesisaac',
  email: 'mailto:masesisaac@gmail.com',
  linkedin: 'https://www.linkedin.com/in/masesisaac/',
}

export const textLinks = [
  {
    href: links.github,
    title: 'Developer',
  }
]
