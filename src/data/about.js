export const what = [
  {
    title: 'Making Applications 💻',
  },
  {
    title: 'Working Remotely 🌏',
  },
]

export const me = {
  name: 'Isaac Maseruka',
  title: 'Fullstack Developer',
}
